// WARNING - THIS FILE HAS BEEN GENERATED. DO NOT EDIT!!!
package dsl5

// initial, object-based implementation
class Test7 {
	companion object {
		@JvmStatic
		fun main(args: Array<String>) {
			// CONFIGURATION
			val doorClosed = Event("doorClosed", "D1CL")
			val drawerOpened = Event("drawerOpened", "D2OP")
			val lightOn = Event("lightOn", "L1ON")
			val doorOpened = Event("doorOpened", "D1OP")
			val panelClosed = Event("panelClosed", "PNCL")

			val unlockPanel = Command("unlockPanel", "PNUL")
			val lockPanel = Command("lockPanel", "PNLK")
			val lockDoor = Command("lockDoor", "D1LK")
			val unlockDoor = Command("unlockDoor", "D1UL")
			
			val idle = State()
			idle.actions.add(unlockDoor)
			idle.actions.add(lockPanel)
			
			val active = State()
			
			val waitingForLight = State()
			
			val waitingForDrawer = State()
			
			val unlockedPanel = State()
			unlockedPanel.actions.add(unlockPanel)
			unlockedPanel.actions.add(lockDoor)
			
			
			idle.transitions[doorClosed] = active
			
			active.transitions[drawerOpened] = waitingForLight
			active.transitions[lightOn] = waitingForDrawer
			
			waitingForLight.transitions[lightOn] = unlockedPanel
			
			waitingForDrawer.transitions[drawerOpened] = unlockedPanel
			
			unlockedPanel.transitions[panelClosed] = idle
			
			
			val machine = StateMachine(idle)
			machine.resetEvents.add(doorOpened)
			
			// test run
			val controller = Controller(idle, machine, CommandChannel())

			println()
			println("TEST t1")
			println("======")
			controller.handle(doorClosed)
			controller.handle(drawerOpened)
			controller.handle(lightOn)
			controller.handle(panelClosed)
			
			println()
			println("TEST t2")
			println("======")
			controller.handle(doorClosed)
			controller.handle(lightOn)
			controller.handle(drawerOpened)
			controller.handle(panelClosed)
			
			println()
			println("TEST t3")
			println("======")
			controller.handle(doorClosed)
			controller.handle(doorOpened)
			
			println()
			println("TEST t4")
			println("======")
			controller.handle(doorClosed)
			controller.handle(lightOn)
			controller.handle(doorOpened)
			
			println()
			println("TEST t5")
			println("======")
			controller.handle(doorClosed)
			controller.handle(lightOn)
			controller.handle(drawerOpened)
			controller.handle(doorOpened)
			
		}
	}
}
